import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs'

interface Credentials {
  user_mail: string,
  user_password: string
}

@Injectable({
  providedIn: 'root'
})
class CredentialsService {

  private tokenSubject: BehaviorSubject<string | undefined>;
  public tokenSuscription: Observable<string>;
  private user: Credentials | undefined;
  private token: string | undefined;

  constructor() {
    this.tokenSubject = new BehaviorSubject(this.token);
    this.tokenSuscription = this.tokenSubject.asObservable();
  }

  setUserInfo(info: { userInfo?: Credentials, token?: string }) {
    if (info.userInfo) this.user = info.userInfo;
    if (info.token) {
      this.token = info.token;
      this.tokenSubject.next(this.token);
    }
  }

  getToken(): string {
    return this.token;
  }

}

export const credentials = new CredentialsService();
