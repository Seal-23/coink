import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment";
import { CryptoService } from "./crypto.service";
import { credentials } from "./credentials.service";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  token: string;

  constructor(private http: HttpClient, private crypto: CryptoService) {
    this.token = "";
    credentials.tokenSuscription.subscribe((token) => {
      this.token = token;
    })
  }

  post(endPoint?: string, data?: any): Observable<any> {
    const payload = this.crypto.encrypt(JSON.stringify(data))
    const config = !!this.token ? { headers: { Authorization: this.token } } : {}
    return this.http.post(
      `${environment.apiUrl}/${endPoint}?apiKey=${environment.apiKey}`,
      JSON.stringify({ payload: payload }),
      config
    ).pipe(map((res: any) => { 
      const data= res.payload;
      return !!data ? JSON.parse(this.crypto.decrypt(data)) : res }));
  }
}
