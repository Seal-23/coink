import { Component } from '@angular/core';
import { Router, Event, NavigationStart } from '@angular/router';
import { credentials } from './services/credentials.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'coink';

  constructor(private router: Router) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        if (!credentials.getToken() && event.url!='/login'){
          this.router.navigate(['login'])
        }
      }
    })
  }
}
