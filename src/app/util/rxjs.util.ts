import { HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';


interface retryHttpConfig {
  maxRetryAttempts?: number,
  scalingDuration?: number,
  scalingIncreaseFactor?: number,
  excludedStatusCodes?: Array<number>
}

const retryHttpDefaultConfig: retryHttpConfig = {
  maxRetryAttempts: 3,
  scalingDuration: 5000,
  scalingIncreaseFactor: 1,
  excludedStatusCodes: []
}

export function retryHttpStrategy(config_: retryHttpConfig = retryHttpDefaultConfig): (error) => Observable<any> {
  const config = { ...retryHttpDefaultConfig, ...config_ };
  return (attempts: Observable<HttpErrorResponse>) => {
    return attempts.pipe(
      switchMap((error: HttpErrorResponse, attemptCount: number) => {
        if (config.excludedStatusCodes.find((code) => code == error.status)
          || attemptCount >= config.maxRetryAttempts) {
          return throwError(error);
        } else {
          return timer(config.scalingDuration * (1 + config.scalingIncreaseFactor * attemptCount));
        }
      })
    )
  }
}
