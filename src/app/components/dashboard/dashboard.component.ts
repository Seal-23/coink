import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

interface Filter {
  begin_date: Date,
  end_date: Date,
  filter_value: string,
  filter_field: string
}
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  searchForm: FormGroup;
  selectOptions: Array<{ title: string, value: string }>;
  defaulFilterValues: Filter;
  tableHeaders: { [key: string]: string }
  tableInfo: { [page: number]: { data: Array<{ [key: string]: string | number }>, loading: boolean } };
  currentPage: number;
  totalPages: number;
  itemsPerPage: number;
  downloadingFile: boolean;

  constructor(private api: ApiService, private formBuilder: FormBuilder) {
    this.defaulFilterValues = {
      begin_date: this.previosMouthDate(),
      end_date: new Date(),
      filter_field: "",
      filter_value: ""
    }
    this.searchForm = this.formBuilder.group({
      begin_date: [this.defaulFilterValues.begin_date, [Validators.required]],
      end_date: [this.defaulFilterValues.end_date, [Validators.required]],
      filter_field: [this.defaulFilterValues.filter_field],
      filter_value: [this.defaulFilterValues.filter_value],
    });
    this.selectOptions = [];
    this.tableInfo = { 1: { data: [], loading: true } };
    this.currentPage = 1;
    this.itemsPerPage = 10;
    this.downloadingFile = false;

  }

  ngOnInit(): void {
    this.getPurchases();
  }

  getPurchases(): void {
    const page = this.currentPage;
    this.tableInfo[page] = { data: [], loading: true };
    this.api.post('pockets/reports/transactions/purchases', {
      vault_id: "b73bde9f-6891-4b2e-847e-484be1830794",
      page: this.currentPage - 1,
      items_per_page: this.itemsPerPage,
      ...this.searchForm.value
    }).subscribe((purchase) => {
      if (!this.tableHeaders) {
        this.tableHeaders = purchase.headers;
        this.selectOptions = [];
        for (const headerName in this.tableHeaders) {
          this.selectOptions.push({ title: headerName, value: headerName })
        }
      }
      if (!this.totalPages) this.totalPages = Math.ceil(purchase.count / this.itemsPerPage);
      this.tableInfo[page] = { data: purchase.items, loading: false }
    })
  }

  search() {
    if (!!this.searchForm.value.filter_value && !this.searchForm.value.filter_field) {
      Swal.fire({
        text: "Seleccione un campo porfavor",
        icon: 'info'
      })
    } else {
      this.getPurchases();
    }
  }

  clearFilters() {
    this.searchForm.setValue(this.defaulFilterValues);
    this.search();
  }

  dateRangeString(): string {
    return `${this.searchForm.value.begin_date.toLocaleDateString()} - ${this.searchForm.value.end_date.toLocaleDateString()}`
  }

  previosMouthDate(): Date {
    const date = new Date();
    date.setMonth(date.getMonth() - 1);
    return date;
  }

  nextPage(): void {
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.getPurchases();
    }
  }

  previousPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getPurchases();
    }
  }

  selectPage(page: number): void {
    this.currentPage = page;
    this.getPurchases();
  }

  headerKeys(): Array<string> {
    if (!!this.tableHeaders) {
      const keyOrder = ['Nombre', 'Cel', 'Email', 'Saldo en compras', 'Edad']
      return Object.keys(this.tableHeaders).sort((a, b) => keyOrder.indexOf(a) - keyOrder.indexOf(b))
    } else {
      return []
    }
  }

  posiblePages(): Array<number> {
    const pages = [];
    const pageToView = 5; //Odd number
    const selectorPageSize: number = this.totalPages > pageToView ? pageToView : this.totalPages;
    let begin_page = 1;
    if ((this.totalPages > pageToView) && this.currentPage > Math.floor(pageToView / 2)) {
      const reference = this.totalPages - this.currentPage;
      if (reference > Math.floor(pageToView / 2)) begin_page = this.currentPage - Math.floor(pageToView / 2);
      else begin_page = this.currentPage + reference - 2 * Math.floor(pageToView / 2);
    }
    for (let page = begin_page; page < (begin_page + selectorPageSize); page++) {
      pages.push(page)
    }
    return pages
  }

  s2ab(s: string): ArrayBuffer {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

  downloadFile(): void {
    if (!this.downloadingFile) {
      this.downloadingFile = true;
      this.api.post('pockets/reports/transactions/purchases/export', {
        vault_id: "b73bde9f-6891-4b2e-847e-484be1830794",
        ...this.searchForm.value
      }).subscribe((report) => {
        const bin = atob(report);
        const ab = this.s2ab(bin); // from example above
        const blob = new Blob([ab], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = `Reporte ${this.dateRangeString()}.csv`;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        this.downloadingFile = false;
      })
    }
  }
}
