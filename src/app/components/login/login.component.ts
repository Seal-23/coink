import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { tap, catchError, retryWhen } from 'rxjs/operators';
import { retryHttpStrategy } from '../../util/rxjs.util'
import { of } from 'rxjs';
import { credentials } from '../../services/credentials.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 8000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
  loginForm: FormGroup;
  securityForm: FormGroup;
  loadingLogin: boolean;
  loadingValidate: boolean;
  isLogin: boolean;


  constructor(private api: ApiService, private formBuilder: FormBuilder, private router: Router) {
    this.loginForm = this.formBuilder.group({
      user_mail: ['', [Validators.required, Validators.email]],
      user_password: ['', [Validators.required, Validators.minLength(8)]]
    });
    this.securityForm = this.formBuilder.group({
      code: [null, [Validators.required, Validators.min(100000), Validators.max(999999)]]
    });
    this.securityForm.controls.code.valueChanges.subscribe((value) => {

    })
    this.loadingLogin = false;
    this.loadingValidate = false;
    this.isLogin = false;
  }

  ngOnInit(): void {
    credentials.tokenSuscription.subscribe((token) => {
      this.isLogin = !!token;
    })
  }

  login(): void {
    this.loadingLogin = true;
    this.api.post('login', this.loginForm.value).pipe(
      tap((token: string) => {
        this.loadingLogin = false;
        credentials.setUserInfo({ userInfo: this.loginForm.value, token })
      }),
      retryWhen(retryHttpStrategy({ excludedStatusCodes: [401, 403] })),
      catchError((error: HttpErrorResponse) => {
        switch (error.status) {
          case 401:
            this.toast.fire({
              text: 'La informacion no corresponde',
              title: 'Error:',
              icon: 'info'
            })
            break;
          case 403:
            this.toast.fire({
              text: 'Usuario bloqueado',
              title: 'Error:',
              icon: 'error'
            })
            break;
          default:
            Swal.fire({
              title: "Error",
              text: "Estamos experimentando problemas técnicos, de persistir comuníquese con soporte",
              icon: 'error'
            })
            break;
        }
        this.loadingLogin = false
        return of({})
      })
    ).subscribe();
  }

  validateCode(): void {
    this.loadingValidate = true;
    setTimeout(() => {
      this.loadingValidate = false;
      this.router.navigate(['dashboard']);
    }, 1000)
  }
}
